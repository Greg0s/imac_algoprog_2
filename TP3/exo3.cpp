#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        //mettre la plus petite à gauche, la plus grande à droite
        if(this->left == nullptr){
            this->left = new SearchTreeNode(value);
        }else if(this->right == nullptr){
            this->right = new SearchTreeNode(value);
        }else{
            if(value < this->value){
                this->left->insertNumber(value);
            }else{
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint countLeft, countRight = 0;
        if(this->isLeaf()){//left and right null
            return 1;
        }
        else if(this->left == nullptr){//left null
            return this->right->height() + 1;
        }
        else if(this->right == nullptr){//right null
            return this->left->height() + 1;
        }
        else{//left and right not null
            countLeft = this->left->height();
            countRight = this->right->height();
            return std::max(countLeft, countRight) + 1;
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if(this->isLeaf()){//left and right null
            return 1;
        }
        else if(this->left == nullptr){//left null
            return this->right->nodesCount() + 1;
        }
        else if(this->right == nullptr){//right null
            return this->left->nodesCount() + 1;
        }
        else{//left and right not null
            return this->left->nodesCount() + this->right->nodesCount() + 1;
        }
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->left == nullptr && this->right == nullptr){
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        ///!\ Ajouter que les feuilles !
        if(this->isLeaf()){//left and right null
            leaves[leavesCount] = this;
            leavesCount++;
            return;
        }
        else {
            if(this->left == nullptr){//left null
                this->right->allLeaves(leaves, leavesCount);
            }
            else if(this->right == nullptr){//right null
                this->left->allLeaves(leaves, leavesCount);
            }
            else{//left and right not null
                this->left->allLeaves(leaves, leavesCount);
                this->right->allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->left != nullptr){
            this->left->inorderTravel(nodes,nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
        if(this->right != nullptr){
            this->right->inorderTravel(nodes,nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;
        if(this->left != nullptr){
            this->left->preorderTravel(nodes,nodesCount);
        }
        if(this->right != nullptr){
            this->right->preorderTravel(nodes,nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(this->left != nullptr){
            this->left->postorderTravel(nodes,nodesCount);
        }
        if(this->right != nullptr){
            this->right->postorderTravel(nodes,nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if(this->value == value){
            return this;
        }
        if(this->left != nullptr){
            this->left->find(value);
        }
        if(this->right != nullptr){
            this->right->find(value);
        }
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
