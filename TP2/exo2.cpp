#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
    sorted[0]=toSort[0];
    uint count = 1;
    uint j;
    bool loop;

    for (uint i=1; i<toSort.size(); i++){
        loop= true;
        j = 0;
        while(j<count && loop==true){
            if(toSort[i]<sorted[j]){
                sorted.insert(j, toSort[i]);
                loop = false;
            }
            j++;
        }
        if(loop == true){
            sorted[count]=toSort[i];
        }
        count++;
    }
	
    toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
