#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
    int temp;
    for (uint j=1 ; j<toSort.size() ; j++){
        for (uint i=1 ; i<toSort.size() ; i++){
            if(toSort[i-1]>toSort[i]){
                temp = toSort[i];
                toSort[i] = toSort[i-1];
                toSort[i-1] = temp;
            }
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
