#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    //stop statement
    if(origin.size() < 2){
        return;
    }

    // initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    int size = origin.size();

    for(uint i=0 ; i<size/2 ; i++){
        first[i]=origin[i];
    }

    for(uint i=0 ; i<size-first.size() ; i++){
        second[i]=origin[i+first.size()];
    }

    //recursive call of splitAndMerge
    splitAndMerge(first);
    splitAndMerge(second);

	// merge

    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    int i =0;
    int j = 0;
    while(i+j<result.size()){
        if(i == first.size()){
            result[i+j] = second[j];
            j++;
        }else if(j == second.size()){
            result[i+j] = first[i];
            i++;
        }else if(first[i] <= second[j]){
            result[i+j] = first[i];
            i++;
        }else if(second[j] < first[i]){
            result[i+j] = second[j];
            j++;
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
