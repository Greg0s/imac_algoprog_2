#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i] = value;
    while(i>0 && (*this)[i] > (*this)[(i-1)/2]){
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    bool noRightChild = true;
    bool noLeftChild = true;
    int rightChildValue;
    int leftChildValue;
    int max;//max value
    int maxI;//max index

    //check if node has child(s)
    if(this->rightChild(nodeIndex) < heapSize){
        rightChildValue = (*this)[this->rightChild(nodeIndex)];
        noRightChild = false;
    }
    if(this->leftChild(nodeIndex) < heapSize){
        leftChildValue = (*this)[this->leftChild(nodeIndex)];
        noLeftChild = false;
    }

    //find the max value between node and child(s)
    if(noLeftChild && noRightChild){//no child
        max = (*this)[nodeIndex];
        maxI = nodeIndex;
    }else if(noLeftChild){//no left child
        max = std::max((*this)[nodeIndex],rightChildValue);
        if(max == (*this)[nodeIndex]){
            maxI = nodeIndex;
        }else{
            maxI = this->rightChild(nodeIndex);
        }
    }else if(noRightChild){//no right child
        max = std::max((*this)[nodeIndex],leftChildValue);
        if(max == (*this)[nodeIndex]){
            maxI = nodeIndex;
        }else{
            maxI = this->leftChild(nodeIndex);
        }
    }else{//left and right child
        max = std::max((*this)[nodeIndex],rightChildValue);
        max = std::max(max, leftChildValue);
        if(max == (*this)[nodeIndex]){
            maxI = nodeIndex;
        }else if(max == (*this)[this->rightChild(nodeIndex)]){
            maxI = this->rightChild(nodeIndex);
        }else{
            maxI = this->leftChild(nodeIndex);
        }
    }

    //swap and recursive call if max is not node
    if(maxI != nodeIndex){
        swap(nodeIndex,maxI);
        heapify(heapSize, maxI);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i = 0 ; i < numbers.size() ; i++){
        this->insertHeapNode(numbers.size(), numbers[i]);
    }
}

void Heap::heapSort()
{
    for(int i = this->size()-1 ; i > 0 ; i--){
        swap(0,i);
        heapify(this->size(), 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
