#include <iostream>
#include <cmath>
using namespace std;

struct Point{
    int x;
    int y;
};

int power(int value, int n);
int fibonacci(int n);
int search(int value, int array[], int size);
void allEvens(int evens[], int array[], int *evenSize, int arraySize);
void afficherTab(int * tab, int size);
bool mandelbrot(Point z, Point point, int n);

int main(){
    cout << power(4,3) << endl;
    cout << fibonacci(5) << endl;
    int tab[5]={3,52,4,9,3};
    cout << search(4, tab, 5) << endl;
    int size_even = 0;
    int even[size_even];
    cout << "-------" << endl;
    afficherTab(tab, 5);
    cout << "-------" << endl;
    allEvens(even, tab, &size_even, 5);
    afficherTab(even, size_even);
    return 0;
}

bool mandelbrot(Point z, Point point, int n){
    if(n>-1){
        int module;
        module=sqrt(z.x*z.x+z.y*z.y);
        if(module<2){
            return true;
        }else{
            int sauv = z.x;
            z.x=(z.x*z.x-z.y*z.y)+point.x;
            z.y=(2*sauv*z.y)+point.y;
            return mandelbrot(z,point,n-1);
        }
    }
    return false;
}

void afficherTab(int * tab, int size){
    for(int i=0;i<size;i++){
        cout << tab[i] << " | ";
    }
    cout << endl;
}

int power(int value, int n){
    if(n>=0){
        if(n==0){
            return 1;
        }
        return value*power(value,n-1);
    }
    return 0;
}

int fibonacci(int n){
    if(n==0 || n==1){
        return n;
    }
    return fibonacci(n-1)+fibonacci(n-2);
}

int search(int value, int array[], int size){
    if(size<0){
        return -1;
    }
    if(array[size-1]==value){
        return size-1;
    }
    return search(value, array, size-1);
}

void allEvens(int evens[], int array[], int *evenSize, int arraySize){
    if(arraySize>0){
        if(array[arraySize-1]%2==0){
                evens[*evenSize]=array[arraySize-1];
                (*evenSize)++;
        }
        arraySize--;
        return allEvens(evens,array,evenSize,arraySize);
    }
}