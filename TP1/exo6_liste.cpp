#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
};

void initialise(Liste* liste)
{
    liste->premier=nullptr;
}

bool est_vide(const Liste* liste)
{
    return (liste->premier == nullptr);
}

void ajoute(Liste* liste, int valeur)
{
    Noeud * nvNoeud = new Noeud;

    if(!nvNoeud){
        std::cout << "Erreur" << endl;
    }

    nvNoeud -> donnee = valeur;
    nvNoeud -> suivant = nullptr;

    if(est_vide(liste)){
        liste->premier = nvNoeud;
    }else{
        Noeud * enCours = liste->premier;
        
        while(enCours->suivant!=nullptr){
            enCours = enCours->suivant;
        }
        enCours->suivant = nvNoeud;
    }    
}

void affiche(const Liste* liste)
{
    Noeud * enCours = liste->premier;
    while(enCours!=nullptr){
        std::cout << enCours->donnee << " | ";
        enCours = enCours->suivant;
    }
    std::cout << endl;
}

int recupere(const Liste* liste, int n)
{
    int cpt = 0;
    Noeud * enCours = liste->premier;
    while(enCours!=nullptr && cpt < n){
        enCours = enCours->suivant;
        cpt ++;
    }
    if(cpt==n){
        return enCours->donnee;
    }
    return -1;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud * enCours = liste->premier;
    int cpt = 1;
    while(enCours!=nullptr){
        if(enCours->donnee==valeur){
            return cpt;
        }
        enCours = enCours->suivant;
        cpt ++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud * enCours = liste->premier;
    int cpt = 1;
    while(enCours!=nullptr){
        if(cpt==n){
            enCours->donnee = valeur;
        }
        enCours = enCours->suivant;
        cpt ++;
    }
}


void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

int retire_file(Liste* liste)
{
    int sauv = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return sauv;
}

void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

int retire_pile(Liste* liste)
{
    if(liste->premier->suivant == nullptr){
        int first = liste->premier->donnee;
        liste->premier = nullptr;
        return first;
    }
    Noeud *enCours = new Noeud;
    enCours = liste->premier;
    while(enCours->suivant->suivant != nullptr){
        enCours = enCours->suivant;
    }
    int sauv = enCours->suivant->donnee;
    enCours->suivant = nullptr;
    return sauv;
}


int main()
{
    Liste liste;
    initialise(&liste);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    
    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;

    stocke(&liste, 4, 7);
    
    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    std::cout << std::endl;
    
    Liste pile;
    Liste file;

    initialise(&pile);
    initialise(&file);
    
    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }
    
    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
