#include <iostream>

using namespace std;

struct DynaTableau
{
    int *donnees;
    int capacite; //nb max
    int taille;   //nb reel
};

void initialise(DynaTableau *tableau, int capacite)
{
    tableau->donnees = new int[capacite];
    tableau->capacite = capacite;
    tableau->taille = 0;
}

void ajoute(DynaTableau *tableau, int valeur)
{
    if (tableau->taille < tableau->capacite)
    {
        tableau->donnees[tableau->taille] = valeur;
        tableau->taille+=1;
    }
    else
    {
        int *tempTab = new int[tableau->capacite+1];
        for(int i=0 ; i<tableau->taille ; i++){
            tempTab[i]=tableau->donnees[i];
        }
        delete tableau->donnees;
        tableau->donnees = tempTab;
        tableau->donnees[tableau->taille] = valeur;
        tableau->capacite+=1;
        tableau->taille+=1;
        
    }
}

bool est_vide(const DynaTableau *liste)
{
    return (liste->taille == 0);
}

void affiche(const DynaTableau *tableau)
{
    for (int i = 0; i < tableau->taille; i++)
    {
        cout << tableau->donnees[i] << " | ";
    }
    cout << endl;
}

int recupere(const DynaTableau *tableau, int n)
{
    return tableau->donnees[n];
}

int cherche(const DynaTableau *tableau, int valeur)
{
    for (int i = 0; i < tableau->taille; i++)
    {
        if (tableau->donnees[i] == valeur)
        {
            return i+1;
        }
    }
    return -1;
}

void stocke(DynaTableau *tableau, int n, int valeur)
{
    tableau->donnees[n-1] = valeur;
}

int main()
{
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&tableau, i*5);
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;
    
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;
    
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;
    
    stocke(&tableau, 4, 7);
    
    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;
    
    /*
    DynaTableau pile;
    DynaTableau file;

    initialise(&pile, 5);
    initialise(&file, 5);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    */

    return 0;
}
